# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError

class FleetReportWizard(models.TransientModel):
    _name = 'wizard.cars.fleet.fleet.assets.report'
    _description = 'A wizard for filter the results for the report'

    date_from = fields.Date(required=True)
    date_to = fields.Date(required=True)

    @api.onchange('date_from')
    def _onchange_date_from(self):
        self._validate_dates_values()

    @api.onchange('date_to')
    def _onchange_date_to(self):
        self._validate_dates_values()

    def _validate_dates_values(self):
        if not self.date_from or not self.date_to:
            return

        date_from : str = self.date_from.strftime('%Y-%m-%d')
        date_to : str = self.date_to.strftime('%Y-%m-%d')

        if date_from > date_to:
            raise ValidationError('The Date From can not be gratter than Date To')

    def action_print_pdf_report(self):        
        data = {
            'domain': {
                'date_from': self.date_from,
                'date_to': self.date_to,
            },
        }

        return self.env.ref('cars_fleet.action_report_fleet_assets').report_action(self, data=data)