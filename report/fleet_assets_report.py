# -*- coding: utf-8 -*-

from odoo import api, fields, models

class FleetAssetsReport(models.AbstractModel):
    _name = 'report.cars_fleet.report_fleet_assets'
    _description = 'Fleet Assets Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        domain : dict = data['domain']
        
        cars_for_report = self.env['cars.fleet.car'].search([
            ('purchase_date', '>=', domain['date_from']),
            ('purchase_date', '<=', domain['date_to']),
        ], order='purchase_date desc')

        return {
            'domain' : domain,
            'fleet_data' : cars_for_report,
        }
