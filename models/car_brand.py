# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CarBrand(models.Model):
    _name = 'cars.fleet.car.brands'
    _description = 'Car Brands'

    name = fields.Char(required=True)
    description = fields.Text()
