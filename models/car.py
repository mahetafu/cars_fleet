# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from math import floor, ceil

class Car(models.Model):
    _name = 'cars.fleet.car'
    _description = 'A Car entity form the fleet'

    name = fields.Char(string='Plate', required=True)
    model_id = fields.Many2one('cars.fleet.car.models', required=True)
    brand = fields.Char(related='model_id.brand_id.name', string='Brand', readonly=True)
    purchase_date = fields.Date(required=True)
    purchase_price = fields.Float(required=True)
    mileage = fields.Integer(required=True)
    actual_price = fields.Float()
    maintenances = fields.Integer(compute='_compute_maintenances', store=True)
    description = fields.Text()

    @api.onchange('mileage')
    def _onchange_mileage(self):
        self._update_actual_price()

    @api.onchange('purchase_price')
    def _onchange_purchase_price(self):
        self._update_actual_price()

    @api.onchange('purchase_date')
    def _onchange_purchase_date(self):
        if not self.purchase_date:
            return

        date_format : str = '%Y-%m-%d'

        today : str = fields.Date.today().strftime(date_format)
        purchase_date : str = self.purchase_date.strftime(date_format)

        if purchase_date > today:
            raise ValidationError('Purchase Date can not be gratter than Today')

    @api.depends('purchase_date')
    def _compute_maintenances(self):
        months_per_maintenance : int = 6

        for record in self:
            record.maintenances = floor(record._months_between_dates(fields.Date.today(), record.purchase_date) 
                / months_per_maintenance)

    def _update_actual_price(self):
        self.actual_price = self._obtain_actual_price() if self.mileage else self.purchase_price

    def _obtain_actual_price(self):
        miles_per_reduction : float = 10000

        times_to_apply_reduction : float = ceil(self.mileage / miles_per_reduction)
        discount_percentage : float = (0.05 * times_to_apply_reduction)
        discount_amount : float = ((self.purchase_price * discount_percentage) / 1)

        return self.purchase_price - discount_amount

    def _months_between_dates(self, date1, date2):
        return abs((date1.year - date2.year) * 12 + date1.month - date2.month) if date1 and date2 else 0