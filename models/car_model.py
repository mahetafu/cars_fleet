# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CarModel(models.Model):
    _name = 'cars.fleet.car.models'
    _description = 'A Car Model'

    name = fields.Char(required=True)
    brand_id = fields.Many2one('cars.fleet.car.brands', required=True)
    description = fields.Text()
